package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class StackImplementation implements StackOperations {

    private final int PEAK_OF_STACK = 0;
    List<String> myStack= new ArrayList<>();

    @Override
    public List<String> get() {
        return myStack;
    }

    @Override
    public Optional<String> pop() {
        if(myStack.isEmpty()){
            return Optional.empty();
        }
        return Optional.of(myStack.remove(PEAK_OF_STACK));
    }

    @Override
    public void push(String item) {
        myStack.add(PEAK_OF_STACK,item);
    }
}
