package com.company;
public class Main {

    public static void main(String[] args) {
	    StackOperations stack = new StackImplementation();
        stack.push("ala");
        stack.push("ma");
        stack.push("kota");
        stack.push("kot");
        stack.push("ma");
        stack.push("ale");

        System.out.println("Stos: "+stack.get());
        System.out.println("Usuwamy element ze stosu: "+stack.pop());
        System.out.println("Stos: "+ stack.get());
    }
}
